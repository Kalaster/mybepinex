#Start The Server
start () {
	sudo systemctl start valheim.service
	echo Valheim Server should be starting soon... Use command "status" to check
	return
}

#Stop the server
stop () {
	echo Halting server
	sudo systemctl stop valheim.service
	return
}

#Status of the Server
status () {
	sudo systemctl status valheim.service
	return
}

#Restart The Server
restart () {
	echo Server is Restarting...
	sudo systemctl restart valheim.service
	return
}

###ALIASES###
alias sour='source .bash_aliases && source .bashrc'
alias alied='nano .bash_aliases'

alias c='clear'

###PACKAGE MANAGEMENT###
alias upgrade='sudo apt update && sudo apt upgrade -y && sudo apt autoremove && sudo flatpak update -y'
alias upd='sudo apt update && apt list --upgradable'
alias upg='sudo apt upgrade -y && sudo flatpak update -y && sudo apt autoremove'
alias aptin='sudo apt install'
alias apt='sudo apt'

alias ll='ls -alF --color=auto'
alias la='ls -A --color=auto'
alias l='ls -CF --color=auto'
alias ls='ls -lah --color=auto'
alias lt='ls --human-readable --size -1 -S --classify'

alias cdh='cd ~'
alias cdg='cd ~/Git_Lab'
alias ..='cd .. && pwd'
alias ...='cd ../.. && pwd'
alias ....='cd ../../.. && pwd'

alias mkdir='mkdir -pv'

###Search BASH History###
alias gh='history | grep'

alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'

alias mkg='mkdir ~/Git_Lab'

alias ping6='ping c 6'

alias uptime='uptime -p'

alias systemctl='sudo systemctl'

alias cg='cd `git rev-parse --show-toplevel`'

###Valheim Server Specific Aliases###
#alias start='cd /home/steam/valheimserver && bash /home/steam/valheimserver/start_server_bepinex.sh'
#alias update='steamcmd +force_install_dir ~/valheimserver +login anonymous +app_update 896660 validate +quite'
alias backup='scp -r /home/steam/valheimserver/Worlds commander@192.168.1.159:/home/commander/Valheim_server_backups/World_Backup-$(date +%F)'
alias log='journalctl --unit=valheim --reverse'
alias logerror='journalctl --unit=valheimserver --reverse | grep error'
alias cdv='cd /home/steam/valheimserver/'
alias valheiem-update='bash /home/steam/valheimserver/update.sh'

