#!/bin/bash

echo installing Valheim server...

sleep 1

echo Updating Distro...
#Updates
apt update &&  apt upgrade -y

echo Updated. Now adding the Multiverse repository

sleep 1
#adding Multi-verse Repository
add-apt-repository multiverse

echo Done. Now adding i386 architecture

sleep 1
#add i386 Architecture
dpkg --add-architecture i386

echo Done. Updating again...

sleep 1
#updating a second time
apt update

echo Done. Now installing steamcmd net-tools and neofetch

sleep 3
#installing Steam Command, Net-tools (for ifconfig/ IP), and neofetch just cuz... 
apt install -y steamcmd net-tools neofetch

echo Done. Creating steam user...

sleep 1

#creating Steam user
useradd -m -s /bin/bash steam

echo Done.

echo Now for the users password

#setting Password for Steam user
passwd steam

echo Adding steam user to sudoers group...

sleep 1
#setting steam user to  group
usermod -aG sudo steam

echo Done. Running Steamcmd and installing Valheim server...

sleep 1
#booting steamCMD, and installing the server
steamcmd +force_install_dir /home/steam/valheimserver +login anonymous +app_update 896660 validate +exit

echo Done. MAKING A SYSLINK...

sleep 1
#creating a symlink fro steamcmd into steams Home folder
ln -s /usr/games/steamcmd /home/steam/steamcmd

echo Done. Copying Mod files...

#copying the contents of the BepinEx folder into the valheimserver directory
cp -r server-mods/* /home/steam/valheimserver

#copies Bash files to steam users Home
cp .bash* /home/steam/

echo Done. Adding Valheim service...

sleep 1

#Copying Valheim Service File to System D directory
cp valheim.service /etc/systemd/system/valheim.service

echo Done. Reloading daemon

sleep 1

#Reloading Daemon
systemctl daemon-reload

echo Done. Enabling Valheim Service at startup...

sleep 1

#Enabling Vlaheim to start at startup
systemctl enable valheim.service

echo Done. Bringing you home...

sleep 1 

#Taking user to Steam Home
cd /home/steam

#changing ownership of Home Directory
chown -R steam:steam /home/steam

su - steam

echo Done.

echo Valheim Server is ready. 


